Hipchat Message Parser
========================

Rules:

The parser should take a string message as input, and return the following, as json:

1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are ASCII strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon. (http://hipchat-emoticons.nyh.name)
3. Links - Any URLs contained in the message, along with the page's title.

Example:

Input: 
```
"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
```

Return (string):

```json
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ]
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
    }
  ]
}
```

The module has a dependency on requests 2.4.1
