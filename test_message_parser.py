#!/bin/python

import json
import unittest
import message_parser

class TestMessageParser(unittest.TestCase):
    
    def setUp(self):
        self.parser = message_parser.MessageParser()

    def test_mentions(self):
        message =  '@chris you around?'
        mentions = self.parser._extract_mentions(message)
        self.assertListEqual(mentions, ['chris'])

        message =  'Do you know if @chris123 is around?'
        mentions = self.parser._extract_mentions(message)
        self.assertListEqual(mentions, ['chris123'])

        message =  'Do you know where\'s @chris123?'
        mentions = self.parser._extract_mentions(message)
        self.assertListEqual(mentions, ['chris123'])

        message =  'No idea where to find @chris123.'
        mentions = self.parser._extract_mentions(message)
        self.assertListEqual(mentions, ['chris123'])

        message = 'contact me at ramiro@ramiro.com'
        mentions = self.parser._extract_mentions(message)
        self.assertListEqual(mentions, [])

        message = '@bob @john @rachel_lang @all @here (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016 @robertSmith'
        mentions = self.parser._extract_mentions(message)
        self.assertListEqual(mentions, ['bob', 'john', 'rachel_lang', 'all', 'here', 'robertSmith'])

    def test_emoticons(self):

        message = 'megusta'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, [])

        message = '(megusta)'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, ['megusta'])

        message = 'Good morning! (megusta) (coffee)'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, ['megusta', 'coffee'])

        message = '(megusta)?'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, ['megusta'])

        message = '(me gusta)?'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, [])

        message = '(me(gusta?'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, [])

        message = '(thisemoticoniswaytoolong)'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, [])

        message = '(me(me(me(me(me)(gusta))?'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, ['me', 'gusta'])

        message = '(me(me(me(me(me)(gusta?'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, ['me'])

        message = '@bob @john (success). Such a cool feature; https://twitter.com/jdorfman/status/430511497475670016'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, ['success'])

        message = '(me(me(me(me(12345)?'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, ['12345'])

        message = 'megusta)'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, [])

        message = '(megusta())'
        emoticons = self.parser._extract_emoticons(message)
        self.assertListEqual(emoticons, [])

    def test_links(self):
        message = 'http://nbcolympics.com'
        links = self.parser._extract_links(message)
        self.assertEquals(links[0]['url'], 'http://nbcolympics.com')
        self.assertIn('title', links[0])
        self.assertGreater(len(links[0]['title']), 1)

        message = 'http://localhost/tests'
        links = self.parser._extract_links(message)
        self.assertEquals(links[0]['url'], 'http://localhost/tests')

        message = 'http://www.google.com'
        links = self.parser._extract_links(message)
        self.assertEquals(links[0]['url'], 'http://www.google.com')
        self.assertGreater(len(links[0]['title']), 1)
        self.assertEquals(links[0]['title'], "Google")

        message = 'http://10.0.1.10/resources'
        links = self.parser._extract_links(message)
        self.assertEquals(links[0]['url'], 'http://10.0.1.10/resources')

        message = 'Olympics are starting soon; http://www.nbcolympics.com'
        links = self.parser._extract_links(message)
        self.assertEquals(links[0]['url'], 'http://www.nbcolympics.com')

        message = 'https://www.nbcolympics.com  Olympics are starting soon | www.nbcolympics.com'
        links = self.parser._extract_links(message)
        self.assertEquals(len(links), 2)
        self.assertIn('url', links[0])
        self.assertEquals(links[0]['url'], 'https://www.nbcolympics.com')
        self.assertEquals(links[1]['url'], 'www.nbcolympics.com')

        message = '@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016'
        links = self.parser._extract_links(message)
        self.assertEquals(len(links), 1)
        self.assertEquals(links[0]['url'], 'https://twitter.com/jdorfman/status/430511497475670016')
        self.assertTrue(links[0]['title'].startswith("Justin Dorfman on Twitter:"))

        message = '@bob @john (success) such a cool feature; ftp://test.com/test/test'
        links = self.parser._extract_links(message)
        self.assertEquals(len(links), 1)
        self.assertEquals(links[0]['url'], 'ftp://test.com/test/test')

    def test_parse(self):
        message =  '@chris you around?'
        result = self.parser.parse(message)  
        self.assertIsNotNone(result, 'The result is none or empty')

        result_dict = json.loads(result)
        self.assertIsInstance(result_dict, dict, 'Result is not a dict, is {0}'.format(type(result_dict)))
        self.assertIn('mentions', result_dict)
        self.assertNotIn('emoticons', result_dict)
        self.assertNotIn('links', result_dict)
        self.assertDictEqual(dict(mentions=['chris']), result_dict)

        message = '@bob @john (success) such a cool feature; ftp://test.com/test/test'
        result = self.parser.parse(message)
        result_dict = json.loads(result)
        self.assertIn('mentions', result_dict)
        self.assertListEqual(result_dict['mentions'], ['bob', 'john'])

        self.assertIn('links', result_dict)
        self.assertDictEqual(result_dict['links'][0], {'url':'ftp://test.com/test/test'})

        self.assertIn('emoticons', result_dict)
        self.assertListEqual(result_dict['emoticons'], ['success'])


if __name__ == '__main__':
    unittest.main()