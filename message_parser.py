#!/bin/python

"""
Hipchat Message Parser

The parser takes a string as an input, and returns a json string with the following format:
{
    "mentions": [
        "bob",
        "john"
    ],
    "emoticons": [
        "success"
    ]
    "links": [
        {
            "url": "https://twitter.com/jdorfman/status/430511497475670016",
            "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
        }
    ]
}
"""

import Queue
import json
import re
import requests
import threading

class MessageParser:

    def __init__(self):
        self.mention_re = re.compile(r'(?<=^|(?<=[^\w\.]))@([\w]+)', re.IGNORECASE)
        self.valid_emoticon_re = re.compile(r'^[\w]{1,15}$', re.IGNORECASE)
        self.link_re = re.compile(r'[a-z]+?://[^\s<>"]+|www\.[^\s<>"]+', re.IGNORECASE)
        self.title_re = re.compile(r'<title>(.*)</title>', re.IGNORECASE)

    def _extract_mentions(self, message):
        return re.findall(self.mention_re, message)

    def _is_emoticon_valid(self, emoticon):
        return re.match(self.valid_emoticon_re, emoticon)

    def _extract_emoticons(self, message):
        emoticons = []

        next_open = message.find('(')
        next_close = message.find(')', next_open + 1)

        while(next_open != -1 and next_close != -1):
            next_next_open = message.find('(', next_open + 1)

            if next_next_open == -1 or next_next_open > next_close:
                candidate = message[next_open + 1 : next_close]
                if self._is_emoticon_valid(candidate):
                    emoticons.append(candidate)

            next_open = next_next_open
            next_close = message.find(')', next_open + 1)

        return emoticons

    def _get_titles(self, queue):
        queue_full = True
        while queue_full:
            try:
                link = queue.get(False)
                response = requests.get(link['url'], timeout=5, allow_redirects=True)    
                if response.status_code >= 200 and response.status_code < 400:
                    title_search = re.search(self.title_re, response.text)
                    if title_search:
                        link['title'] = title_search.group(1)

            except Queue.Empty:
                queue_full = False
            except Exception as e:
                # we don't do anything, as it's all based on best effort 
                continue

    def _extract_links(self, message):
        urls = re.findall(self.link_re, message)
        links = [dict(url=url) for url in urls]

        queue = Queue.Queue()

        for link in links:
            if link['url'].startswith('http'):
                queue.put(link)

        thread_count = 3
        thread_list = []
        for i in range(0, thread_count):
            t = threading.Thread(target=self._get_titles, args=(queue,))
            thread_list.append(t)
            t.daemon = True
            t.start()

        for thread in thread_list:
            thread.join()

        return links

    def parse(self, message):
        if not message:
            raise ValueError('Invalid message: {0}'.format(message))
        
        mentions = self._extract_mentions(message)
        emoticons = self._extract_emoticons(message)
        links = self._extract_links(message)

        parsed = dict()
        if len(mentions) > 0: 
            parsed['mentions'] = mentions

        if len(emoticons) > 0: 
            parsed['emoticons'] = emoticons

        if len(links) > 0: 
            parsed['links'] = links

        return json.dumps(parsed)